<?php

namespace App\Traits;

use App\Providers\Contracts\ProviderContract;
use App\Providers\Exceptions\InvalidProviderException;

trait InitiatesProviders
{
    private function makeProviders(): void
    {
        foreach ($this->providerNames as $provider) {
            $this->providers[$provider] = $this->makeProvider($provider);
        }
    }

    private function makeProvider(string $provider): ProviderContract
    {
        $providerName = ucfirst($provider);
        $class = "\App\Providers\\{$providerName}Provider";

        if (false === class_exists($class)) {
            throw new InvalidProviderException();
        }

        return new $class();
    }
}
