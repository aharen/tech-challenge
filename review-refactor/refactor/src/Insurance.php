<?php

namespace App;

use App\Traits\InitiatesProviders;

class Insurance
{
    use InitiatesProviders;

    private ?array $providers = null;

    public function __construct(
        private array $providerNames = ['bank', 'insurance']
    ) {
        $this->makeProviders();
    }

    public function getQuotes(): array
    {
        $quotes = [];
        
        foreach ($this->providers as $providerName => $provider) {
            $quotes[$providerName] = $provider->fetchPrices();
        }

        return $quotes;
    }
}
