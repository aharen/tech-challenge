<?php

namespace App\Providers\Exceptions;

use RuntimeException;

class ConfigurationException extends RuntimeException
{
    public function __construct(
        $message = 'Provided config is invalid',
        $code = 412
    ) {
        parent::__construct($message, $code);
    }
}
