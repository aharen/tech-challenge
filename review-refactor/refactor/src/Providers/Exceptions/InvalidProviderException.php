<?php

namespace App\Providers\Exceptions;

use RuntimeException;

class InvalidProviderException extends RuntimeException
{
    public function __construct(
        $message = 'Invalid Provider',
        $code = 412
    ) {
        parent::__construct($message, $code);
    }
}
