<?php

namespace App\Providers;

class InsuranceProvider extends AbstractProvider
{
    public function setConfig(): void
    {
        $this->config = [
            'url' => 'http://demo9084693.mockable.io/insurance',
            'method' => 'POST',
            'data' => [
                'month' => 1
            ]
        ];
    }
}
