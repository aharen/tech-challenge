<?php

namespace App\Providers;

use App\HttpClient\HttpClient;
use App\Providers\Contracts\ProviderContract;
use App\Providers\Exceptions\ConfigurationException;

abstract class AbstractProvider implements ProviderContract
{
    protected ?array $config = null;

    protected HttpClient $client;

    protected array $requiredConfig = [
        'url',
        'method',
    ];

    public function __construct()
    {
        $this->make();

        $this->makeHttpClient();
    }

    public function fetchPrices()
    {
        $data = $this->config['data'] ?? [];
        return $this->client->request($data);
    }

    public function make()
    {
        $this->setConfig();

        $this->validateConfig();
    }

    private function validateConfig(): void
    {
        // check if all required fields are set by config
        $configDiff = array_diff(
            $this->requiredConfig,
            array_keys($this->config)
        );

        if (count($configDiff) > 0) {
            throw new ConfigurationException(
                'Configuration is missing required parameters: ' . implode($configDiff)
            );
        }
    }

    private function makeHttpClient(): void
    {
        $this->client = new HttpClient(
            $this->config['url'],
            $this->config['method'],
        );
    }
}
