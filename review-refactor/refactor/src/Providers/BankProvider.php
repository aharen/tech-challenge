<?php

namespace App\Providers;

class BankProvider extends AbstractProvider
{
    public function setConfig(): void
    {
        $this->config = [
            'url' => 'http://demo9084693.mockable.io/bank',
            'method' => 'GET',
        ];
    }
}
