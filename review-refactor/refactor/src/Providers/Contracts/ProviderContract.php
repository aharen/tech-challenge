<?php

namespace App\Providers\Contracts;

interface ProviderContract
{
    public function setConfig(): void;

    public function fetchPrices();
}
