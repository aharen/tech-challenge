<?php

namespace App\HttpClient;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class HttpClient
{
    public function __construct(
        private string $url,
        private string $method
    ) {
        $this->client = new Client([
            'base_uri' => $this->url,
        ]);
    }

    public function request(array $data = [])
    {
        try {
            $request =  $this->client->request(
                $this->method,
                $this->url,
                [
                    'form_params' => $data
                ]
            );
            $response = $request->getBody();
    
            return json_decode($response, true);
        } catch (ClientException $e) {
            // Log the error
            return "Error: {$e->getMessage()}";
        } catch (RequestException $e) {
            // Log the error
            return "Error: {$e->getMessage()}";
        }
    }
}
