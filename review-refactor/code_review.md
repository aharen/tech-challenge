## Code review

At it's current state, this code is not ideal for production. There are several points of failure which can lead to service/product distruption and make it difficult to debug. 

These are few point that i have noted below for improvement.

- Please add testing for the code.
- It is too coupled together and can be hard to expand. 

Eg: Introducing a new provider can be a pain point in the near future. Refactor to make use of Dependency Iversion (or any other refactoring suggestions are welcome)

```
if (!$providers) {
    $providers = ['bank','insurance-company'];
} else $providers = [$providers];
$quote = array();
```

That will also help improve the remote calls.

- No error handling done, this could potentialy crash the service. Specially where remote calls are done (`file_get_contents` and `curl` requests)
- Make remote requests consistent, which will make it easier for error handling and testing as well.
- Refactor to use design patterns/best practices which can improve the code readability and maintainability.
- The class does too much, as in, it has too many responsibilities. Extract them out and it will make it so much more expressive and clean.