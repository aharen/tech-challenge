# review-refactor

Please find my code review at [Code Review](code_review.md). 

Please find the refactoring at [Refactored](refactor)

## Setup the refactor

Follow the steps to install the dependencies and get it running in the browser.

```
cd review-refactor/refactor
```

Run composer install

```
composer install
```

Start a PHP dev server

```
php -S localhost:3333 index.php
```

