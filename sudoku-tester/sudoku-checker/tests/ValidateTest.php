<?php

use App\Validation\Validate;
use PHPUnit\Framework\TestCase;

class ValidateTest extends TestCase
{
    private $valid_matrix;

    private $invalid_matrix;

    public function setUp(): void
    {
        $this->valid_matrix = [
            [1,8,2,5,4,3,6,9,7],
            [9,6,5,1,7,8,3,4,2],
            [7,4,3,9,6,2,8,1,5],
            [3,7,4,8,9,6,5,2,1],
            [6,2,8,4,5,1,7,3,9],
            [5,1,9,2,3,7,4,6,8],
            [2,9,7,6,8,4,1,5,3],
            [4,3,1,7,2,5,9,8,6],
            [8,5,6,3,1,9,2,7,4],
        ];

        $this->invalid_matrix = [
            [1,8,2,5,4,3,6,9,9],
            [9,6,5,1,7,8,3,4,2],
            [7,4,3,9,6,2,8,1,5],
            [3,7,4,8,9,6,5,2,1],
            [6,2,8,4,5,1,7,3,9],
            [5,1,9,2,3,7,4,6,8],
            [2,9,7,6,8,4,1,5,3],
            [4,3,1,7,2,5,9,8,6],
            [8,5,6,3,1,9,2,7,4],
        ];
    }

    public function test_rows_valid()
    {
        $validate = new Validate($this->valid_matrix);
        
        $this->assertTrue(
            $validate->rows()
        );
    }

    public function test_rows_invalid()
    {
        $validate = new Validate($this->invalid_matrix);

        $this->assertFalse(
            $validate->rows()
        );
    }

    public function test_columns_valid()
    {
        $validate = new Validate($this->valid_matrix);
        
        $this->assertTrue(
            $validate->columns()
        );
    }

    public function test_columns_invalid()
    {
        $validate = new Validate($this->invalid_matrix);

        $this->assertFalse(
            $validate->columns()
        );
    }

    public function test_boxes_valid()
    {
        $validate = new Validate($this->valid_matrix);
        
        $this->assertTrue(
            $validate->boxes()
        );
    }

    public function test_boxes_invalid()
    {
        $validate = new Validate($this->invalid_matrix);

        $this->assertFalse(
            $validate->boxes()
        );
    }

    public function test_validate_with_valid_data()
    {
        $validate = new Validate($this->valid_matrix);
        
        $this->assertTrue(
            $validate->validate()
        );
    }

    public function test_validate_with_invalid_data()
    {
        $validate = new Validate($this->invalid_matrix);
        
        $this->assertFalse(
            $validate->validate()
        );
    }
}
