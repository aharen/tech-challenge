<?php

namespace App\Validation;

class Validate
{
    public function __construct(
        private array $matrix
    ) {
    }

    public function validate()
    {
        if (false === $this->rows()) {
            return false;
        }

        if (false === $this->columns()) {
            return false;
        }

        if (false === $this->boxes()) {
            return false;
        }
        
        return true;
    }

    private function run(array $item): bool
    {
        $diff = array_diff(
            [1,2,3,4,5,6,7,8,9],
            $item
        );
        
        return count($diff) === 0;
    }

    public function rows(): bool
    {
        foreach ($this->matrix as $row) {
            if (false === $this->run($row)) {
                return false;
            }
            continue;
        }

        return true;
    }

    public function columns(): bool
    {
        for ($i = 0; $i < 9; $i++) {
            $column  = array_column($this->matrix, $i);
            if (false === $this->run($column)) {
                return false;
            }
            continue;
        }

        return true;
    }

    public function boxes(): bool
    {
        $boxes = new Box($this->matrix);

        foreach ($boxes->get() as $box) {
            if (false === $this->run($box)) {
                return false;
            }
            continue;
        }

        return true;
    }
}
