<?php

namespace App\Validation;

class Box
{
    // cordinates to starting boxes of 3x3
    private array $boxCordinates = [
        [0,0],
        [0,3],
        [0,6],
        [3,0],
        [3,3],
        [3,6],
        [6,0],
        [6,3],
        [6,6],
    ];

    public function __construct(
        private array $matrix
    ) {
    }

    public function get(): array
    {
        $out = [];

        foreach ($this->boxCordinates as $box_key => $box) {
            list($startX, $startY) = $box;
            
            for ($x = $startX; $x < $startX + 3; $x++) {
                for ($y = $startY; $y < $startY + 3; $y++) {
                    $out[$box_key][] = $this->matrix[$x][$y];
                }
            }
        }

        return $out;
    }
}
