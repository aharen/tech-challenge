<?php

namespace App;

use App\Validation\Validate;

class Sudoku
{
    public function __construct(
        private array $matrix
    ) {
    }

    public function validate(): string
    {
        $validation = new Validate($this->matrix);

        return true === $validation->validate() ?
            'The sudoku is solved and valid' :
            'The sudoku is unsolved or invalid';
    }
}
