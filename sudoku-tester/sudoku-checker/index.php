<?php

use App\Sudoku;
use App\Validation\Box;
use App\Validation\Validate;

require __DIR__ . '/vendor/autoload.php';

$matrix_solved = [
    [1,8,2,5,4,3,6,9,7],
    [9,6,5,1,7,8,3,4,2],
    [7,4,3,9,6,2,8,1,5],
    [3,7,4,8,9,6,5,2,1],
    [6,2,8,4,5,1,7,3,9],
    [5,1,9,2,3,7,4,6,8],
    [2,9,7,6,8,4,1,5,3],
    [4,3,1,7,2,5,9,8,6],
    [8,5,6,3,1,9,2,7,4],
];

$matrix_unsolved = [
    [0,4,0,5,0,7,0,0,6],
    [0,0,7,0,0,0,2,3,0],
    [2,0,6,1,0,3,0,0,5],
    [0,0,2,3,0,0,0,0,4],
    [0,3,0,6,0,4,0,8,0],
    [6,0,0,0,0,2,1,0,0],
    [7,0,0,8,0,6,3,0,1],
    [0,1,8,0,0,0,5,0,0],
    [9,0,0,7,0,1,0,2,0],
];

echo '<br />';
echo '<br />';
echo 'Solved:';
echo '<br />';
echo '<br />';


$sudoku = new Sudoku($matrix_solved);

var_dump(
    $sudoku->validate()
);

echo '<br />';
echo '<br />';
echo 'Unsolved:';
echo '<br />';
echo '<br />';

$sudoku = new Sudoku($matrix_unsolved);

var_dump(
    $sudoku->validate()
);
