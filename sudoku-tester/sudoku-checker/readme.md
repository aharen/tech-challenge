# Sudoku Challenge Setup

Follow the steps to install the dependencies and get it running in the browser.

```
cd sudoku-tester/sudoku-checker
```

Run composer install

```
composer install
```

Start a PHP dev server

```
php -S localhost:3333 index.php
```