# PHP Sudoku Test

Please find the sudoku challenge code at [Sudoku Challenge](sudoku-checker)

## Thought process

## Initial Idea

The first idea I had is to load the data in to a muli-dimentional array which will give easy access to rows (x-axis). Then create a temp array from that to hold the columns (y-axis). But immediate issues i find with that approach is that data is replicated and could possible have an added recursive loop to generate the column array. Another issue is, this does not address the 3rd constraint, tha is the 3x3 boxes.

Since we're checking if it's solved correctly, the other idea i have is to check the sum of the array using `array_sum`. Which should always be 45 if correctly solved. I think this approach could potentialy simplify things a little bit more and make it run a bit more faster.

As for the 3x3 box, the first idea i have is to use an array map of the index of items mapped to a assigned grid number for the 3x3 box. For example; if in the following matrix

```
[
    [1,2,3,4,5,6,7,8,9]
    [4,5,6,7,8,9,1,2,3]
    [7,8,9,1,2,3,4,5,6]
    [1,2,3,4,5,6,7,8,9]
    [4,5,6,7,8,9,1,2,3]
    [7,8,9,1,2,3,4,5,6]
    [1,2,3,4,5,6,7,8,9]
    [4,5,6,7,8,9,1,2,3]
    [7,8,9,1,2,3,4,5,6]
]
```

The map would look like:

```
[
    '0,0' => 1,
    '0,1' => 1,
    '0,2' => 1,

    '0,3' => 2,
    '0,4' => 2,
    '0,5' => 2,

    '0,6' => 3,
    '0,7' => 3,
    '0,8' => 3,

    '1,0' => 1,
    '1,1' => 1,
    '1,2' => 1,

    '1,3' => 2,
    '1,4' => 2,
    '1,5' => 2,

    '1,6' => 3,
    '1,7' => 3,
    '1,8' => 3,

    ...
]
```

where the key is `x,y`. This will allow the use of the same technique (sum of the array) of verification for all the grid items.

### Issue identified with the approach

However, the issue with using `array_sum` is, it does not verify the uniqueness of the numbers. For instance, the following sequence of numbers would be valid in this approach:

```
[0,3,3,4,5,6,7,8,9]
```

## Revision of idea

**Fetching the columns**. Instead of creating/maintaining a duplicate data source, since the matrix is an array, the following method can be applied to retrieve the column in question:

```
array_column($matrix, 8)
```

where `$matrix` is the sudoku and `8` is the index of the column. Using the previous matrix as an example, this should output:

```
[
    [9]
    [3]
    [6]
    [9]
    [3]
    [6]
    [9]
    [3]
    [6]
]
```

**Verify solution** Instead of doing a `array_sum`, maybe i could do `array_diff`. Where when difference is found the solution is invalid. Eg:

```
$diff = array_diff(
    [1,2,3,4,5,6,7,8,9],
    $item
);
```

where `$item` can be row, column or 3x3 box.

**3x3 box** The map is huge! So instead of creating a mapping for the whole matrix, creating a cordinate based setup makes it much much smaller. So for each of the 9 boxes, cordinates of the starting box is recorded. This makes it possible to iterate over the cordinates to fetch the content of the box. eg:

```
$boxes = [
    [0,0],
    [0,3],
    [0,6],
    [3,0],
    [3,3],
    [3,6],
    [6,0],
    [6,3],
    [6,6],
];
```