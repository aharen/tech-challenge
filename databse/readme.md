# Databse

## Create Database & Tables

For instructions please check the following link.

[Create Database, Tables and Import Data](instructions.md)

## Queries

### Find author by name "Leo"

```
SELECT
	id,
	name,
	country
FROM
	authors
WHERE
	name LIKE '%Leo%';
```

### Find books of author "Fitzgerald"

```
SELECT
	id,
	name,
	author,
	pages
FROM
	books
WHERE
	author LIKE '%Fitzgerald%';
```

### Find authors without books

```
SELECT
	a.id,
	a.name,
	a.country
FROM
	authors AS a
	LEFT JOIN books AS b ON a.name = b.author
WHERE
	b.name IS NULL;
```

### Count books per country

```
SELECT
	a.country,
	COUNT(a.country)
FROM
	authors AS a
	LEFT JOIN books AS b ON a.name = b.author
WHERE
	b.name IS NOT NULL
GROUP BY
	a.country;
```

### Count average book length (in pages) per author

```
SELECT
	author,
	AVG(pages)
FROM
	BOOKS
GROUP BY
	author;
```

## Analysis

Include potential suggestions on how to improve it.
Consider that there might be millions of authors with millions of books.

Potential room for improvements:

### Make use of foreign keys

The countries can be extracted to it's own table and referenced on `authors` table. Same can also be applied for `books` table, where authors can be referenced. 

This will enforce data integrity and reduce data duplication.

Eg: Books table with author foreign key

```
CREATE TABLE books (
	id SERIAL PRIMARY KEY,
	author_id SERIAL,
	name VARCHAR(255),
	pages INT,
	CONSTRAINT fk_authors
		FOREIGN KEY(author_id)
			REFERENCES authors(id)
)
```

### Indexing

Indexing can improve the performance of long-running & slow queries. We can make use of `EXPLAIN ANALYSE` and identify where indices can improve the performance.