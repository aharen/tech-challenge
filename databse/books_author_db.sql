-- create authors table

CREATE TABLE authors (
	id SERIAL PRIMARY KEY,
	name VARCHAR(255) NOT NULL,
	country VARCHAR(255) NOT NULL 
);

-- insert authors data

INSERT INTO authors 
	("name", "country")
VALUES
	('J. D. Salinger', 'US'), 
	('F. Scott. Fitzgerald', 'US'), 
	('Jane Austen', 'UK'), 
	('Leo Tolstoy', 'RU'), 
	('Sun Tzu', 'CN'), 
	('Johann Wolfgang von Goethe', 'DE'), 
	('Janis Eglitis', 'LV');

-- create books table

CREATE TABLE books (
	id SERIAL PRIMARY KEY,
	name VARCHAR(255),
	author VARCHAR(255),
	pages INT
);

-- insert books data
INSERT INTO books
	("name", "author", "pages")
VALUES
	('The Catcher in the Rye', 'J. D. Salinger', 300),
	('Nine Stories', 'J. D. Salinger', 200),
	('Franny and Zooey', 'J. D. Salinger', 150),
	('The Great Gatsby', 'F. Scott. Fitzgerald', 400),
	('Tender is the Night', 'F. Scott. Fitzgerald', 500),
	('Pride and Prejudice', 'Jane Austen', 700),
	('The Art of War', 'Sun Tzu', 128),
	('Faust I', 'Johann Wolfgang von Goethe', 300),
	('Faust II', 'Johann Wolfgang von Goethe', 300);