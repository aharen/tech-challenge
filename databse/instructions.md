The following instructions are for creating database and creating and importing tables with the data.

## Prerequisite

Since for creating and installing we'll be using the `psql` CLI tool, please make sure that you have the tool installed on your system.

- PostgreSQL Server [Download PostgreSQL](https://www.postgresql.org/download/)
- If you'd like to install only `psql` (useful for when running PostgreSQL using docker) here is a good article [Setting up `psql`](https://blog.timescale.com/blog/how-to-install-psql-on-mac-ubuntu-debian-windows/)

Before we can create the database, make sure the server is running and that you can connect to it.

```
psql -h localhost -U postgres
```

Here, after `-h` is the host of your database server and after `-U` comes your username. If your configurations are different you need to update those in order to make a connection.

For more information you can check out [the documentation](https://www.postgresql.org/docs/13/app-psql.html)

## 1. Creating the database

Once you are connected to the database, run the following command to create a database.

```
CREATE DATABASE "books_author_db" ENCODING = 'UTF8';
```

We are creating a database called `books_author_db` with `UTF8` encoding. If the command was successfull you should get a response as follows:

```
CREATE DATABASE
```

You can further confirm it by listing the databases using the list command

```
\l
```

Response:

```
postgres-# \l
                                    List of databases
      Name       |  Owner   | Encoding |  Collate   |   Ctype    |   Access privileges
-----------------+----------+----------+------------+------------+-----------------------
 books_author_db | postgres | UTF8     | en_US.utf8 | en_US.utf8 |
```

## 2. Create tables and import the data

You will find with this document an SQL file `books_author_db.sql`. We're going to create tables and import the data using this file.

First, we need to connect to the database that we just created

```
\c books_author_db
```

Response:

```
postgres-# \c books_author_db
You are now connected to database "books_author_db" as user "postgres".
```

Now run the sql file with `psql`

```
\i path/to/folder/books_author_db.sql
```

If you're having any issues with this command, you can exit out of `psql` using the command `\q` and the run the command as follows.

NOTE: Make sure that you're the file if located in the correct directory

```
psql -h localhost -U postgres -f path/to/folder/books_author_db.sql
```

Response:

```
books_author_db-# \i database.sql
CREATE TABLE
INSERT 0 7
CREATE TABLE
INSERT 0 9
```