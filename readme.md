## Submissions

Please find individual submissions at:

- [Code Review Refactor](review-refactor)
- [Database Tasks](databse)
- [Sudoku Tester](sudoku-tester)

## Work Process in Git

Each tast was treated as a feature so each will have it's respective feature branch which has been merged in to `main`

## Time Spent on Tasks (est.)

- review-refactor: 3 hrs
- database: 1 hr
- sudoku-solver: 5 hrs